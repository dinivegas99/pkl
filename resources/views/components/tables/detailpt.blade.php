<div class="container-fluid">
    <!-- gambar latar belakang -->
    <div class="page-header min-height-300 border-radius-xl mt-4"
        style="background-image: url('../assets/img/curved-images/tambang.jpg'); background-position-y: 100%;">
    </div>
    <!-- data pt -->
    <div class="card card-body blur shadow-blur mx-4 mt-n6">
        <div class="row gx-4">
            <!-- gambar akun -->
            <div class="col-auto">
                <div class="avatar avatar-xl position-relative">
                    <img src="../assets/img/SIP-BANG.png" alt="..." class="w-100 border-radius-lg shadow-sm">
                    <a href="javascript:;"
                        class="btn btn-sm btn-icon-only bg-gradient-light position-absolute bottom-0 end-0 mb-n2 me-n2">
                        <i class="fa fa-pen top-0" data-bs-toggle="tooltip" data-bs-placement="top"
                            title="Edit Image"></i>
                    </a>
                </div>
            </div>
            <!-- nama pt -->
            <div class="col-auto my-auto">
                <div class="h-100">
                    <h5 class="mb-1">
                        PT. RJ Abadi
                    </h5>
                    <p class="mb-0 font-weight-bold text-sm">
                        Pemilik Ijin Wilayah Tambang
                    </p>
                </div>
            </div>
            <!-- tidak terpakai -->
            <!-- <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
                <div class="nav-wrapper position-relative end-0">
                    <ul class="nav nav-pills nav-fill p-1 bg-transparent" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link mb-0 px-0 py-1 active " data-bs-toggle="tab" href="javascript:;"
                                role="tab" aria-controls="overview" aria-selected="true">
                                <svg class="text-dark" width="16px" height="16px" viewBox="0 0 42 42" version="1.1"
                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="Basic-Elements" stroke="none" stroke-width="1" fill="none"
                                        fill-rule="evenodd">
                                        <g id="Rounded-Icons" transform="translate(-2319.000000, -291.000000)"
                                            fill="#FFFFFF" fill-rule="nonzero">
                                            <g id="Icons-with-opacity" transform="translate(1716.000000, 291.000000)">
                                                <g id="box-3d-50" transform="translate(603.000000, 0.000000)">
                                                    <path class="color-background"
                                                        d="M22.7597136,19.3090182 L38.8987031,11.2395234 C39.3926816,10.9925342 39.592906,10.3918611 39.3459167,9.89788265 C39.249157,9.70436312 39.0922432,9.5474453 38.8987261,9.45068056 L20.2741875,0.1378125 L20.2741875,0.1378125 C19.905375,-0.04725 19.469625,-0.04725 19.0995,0.1378125 L3.1011696,8.13815822 C2.60720568,8.38517662 2.40701679,8.98586148 2.6540352,9.4798254 C2.75080129,9.67332903 2.90771305,9.83023153 3.10122239,9.9269862 L21.8652864,19.3090182 C22.1468139,19.4497819 22.4781861,19.4497819 22.7597136,19.3090182 Z"
                                                        id="Path"></path>
                                                    <path class="color-background"
                                                        d="M23.625,22.429159 L23.625,39.8805372 C23.625,40.4328219 24.0727153,40.8805372 24.625,40.8805372 C24.7802551,40.8805372 24.9333778,40.8443874 25.0722402,40.7749511 L41.2741875,32.673375 L41.2741875,32.673375 C41.719125,32.4515625 42,31.9974375 42,31.5 L42,14.241659 C42,13.6893742 41.5522847,13.241659 41,13.241659 C40.8447549,13.241659 40.6916418,13.2778041 40.5527864,13.3472318 L24.1777864,21.5347318 C23.8390024,21.7041238 23.625,22.0503869 23.625,22.429159 Z"
                                                        id="Path" opacity="0.7"></path>
                                                    <path class="color-background"
                                                        d="M20.4472136,21.5347318 L1.4472136,12.0347318 C0.953235098,11.7877425 0.352562058,11.9879669 0.105572809,12.4819454 C0.0361450918,12.6208008 6.47121774e-16,12.7739139 0,12.929159 L0,30.1875 L0,30.1875 C0,30.6849375 0.280875,31.1390625 0.7258125,31.3621875 L19.5528096,40.7750766 C20.0467945,41.0220531 20.6474623,40.8218132 20.8944388,40.3278283 C20.963859,40.1889789 21,40.0358742 21,39.8806379 L21,22.429159 C21,22.0503869 20.7859976,21.7041238 20.4472136,21.5347318 Z"
                                                        id="Path" opacity="0.7"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                <span class="ms-1">Overview</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" href="javascript:;" role="tab"
                                aria-controls="teams" aria-selected="false">
                                <svg class="text-dark" width="16px" height="16px" viewBox="0 0 40 44" version="1.1"
                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <title>document</title>
                                    <g id="Basic-Elements" stroke="none" stroke-width="1" fill="none"
                                        fill-rule="evenodd">
                                        <g id="Rounded-Icons" transform="translate(-1870.000000, -591.000000)"
                                            fill="#FFFFFF" fill-rule="nonzero">
                                            <g id="Icons-with-opacity" transform="translate(1716.000000, 291.000000)">
                                                <g id="document" transform="translate(154.000000, 300.000000)">
                                                    <path class="color-background"
                                                        d="M40,40 L36.3636364,40 L36.3636364,3.63636364 L5.45454545,3.63636364 L5.45454545,0 L38.1818182,0 C39.1854545,0 40,0.814545455 40,1.81818182 L40,40 Z"
                                                        id="Path" opacity="0.603585379"></path>
                                                    <path class="color-background"
                                                        d="M30.9090909,7.27272727 L1.81818182,7.27272727 C0.814545455,7.27272727 0,8.08727273 0,9.09090909 L0,41.8181818 C0,42.8218182 0.814545455,43.6363636 1.81818182,43.6363636 L30.9090909,43.6363636 C31.9127273,43.6363636 32.7272727,42.8218182 32.7272727,41.8181818 L32.7272727,9.09090909 C32.7272727,8.08727273 31.9127273,7.27272727 30.9090909,7.27272727 Z M18.1818182,34.5454545 L7.27272727,34.5454545 L7.27272727,30.9090909 L18.1818182,30.9090909 L18.1818182,34.5454545 Z M25.4545455,27.2727273 L7.27272727,27.2727273 L7.27272727,23.6363636 L25.4545455,23.6363636 L25.4545455,27.2727273 Z M25.4545455,20 L7.27272727,20 L7.27272727,16.3636364 L25.4545455,16.3636364 L25.4545455,20 Z"
                                                        id="Shape"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                <span class="ms-1">Teams</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-0 px-0 py-1 text-right " data-bs-toggle="tab" href="javascript:;" role="tab"
                                aria-controls="dashboard" aria-selected="true">
                                <svg class="text-dark" width="16px" height="16px" viewBox="0 0 40 40" version="1.1"
                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <title>settings</title>
                                    <g id="Basic-Elements" stroke="none" stroke-width="1" fill="none"
                                        fill-rule="evenodd">
                                        <g id="Rounded-Icons" transform="translate(-2020.000000, -442.000000)"
                                            fill="#FFFFFF" fill-rule="nonzero">
                                            <g id="Icons-with-opacity" transform="translate(1716.000000, 291.000000)">
                                                <g id="settings" transform="translate(304.000000, 151.000000)">
                                                    <polygon class="color-background" id="Path" opacity="0.596981957"
                                                        points="18.0883333 15.7316667 11.1783333 8.82166667 13.3333333 6.66666667 6.66666667 0 0 6.66666667 6.66666667 13.3333333 8.82166667 11.1783333 15.315 17.6716667">
                                                    </polygon>
                                                    <path class="color-background"
                                                        d="M31.5666667,23.2333333 C31.0516667,23.2933333 30.53,23.3333333 30,23.3333333 C29.4916667,23.3333333 28.9866667,23.3033333 28.48,23.245 L22.4116667,30.7433333 L29.9416667,38.2733333 C32.2433333,40.575 35.9733333,40.575 38.275,38.2733333 L38.275,38.2733333 C40.5766667,35.9716667 40.5766667,32.2416667 38.275,29.94 L31.5666667,23.2333333 Z"
                                                        id="Path" opacity="0.596981957"></path>
                                                    <path class="color-background"
                                                        d="M33.785,11.285 L28.715,6.215 L34.0616667,0.868333333 C32.82,0.315 31.4483333,0 30,0 C24.4766667,0 20,4.47666667 20,10 C20,10.99 20.1483333,11.9433333 20.4166667,12.8466667 L2.435,27.3966667 C0.95,28.7083333 0.0633333333,30.595 0.00333333333,32.5733333 C-0.0583333333,34.5533333 0.71,36.4916667 2.11,37.89 C3.47,39.2516667 5.27833333,40 7.20166667,40 C9.26666667,40 11.2366667,39.1133333 12.6033333,37.565 L27.1533333,19.5833333 C28.0566667,19.8516667 29.01,20 30,20 C35.5233333,20 40,15.5233333 40,10 C40,8.55166667 39.685,7.18 39.1316667,5.93666667 L33.785,11.285 Z"
                                                        id="Path"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                <span href="" class="ms-1">Edit Akun</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div> -->
        </div>
    </div>
</div>

    <div class="container-fluid py-4 mb-0">
        <div class="row">
            <!-- informasi pt -->
            <div class="col-lg-6">
                <div class="card">
                    <!-- header informasi -->
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-md-8 mb-0">
                                <h5 class="">Profile Information</h5>
                            </div>
                            <div class="col-md-4 text-right">
                                <a href="javascript:;">
                                    <i class="fas fa-user-edit text-secondary text-sm" data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Edit Profile"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- body informasi -->
                    <div class="card-body">
                        <ul class="list-group">
                        <li class="row col-12">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <strong class="text-left text-sm text-dark"> Nama PT </strong>
                                </div>
                            </div>
                            <div class="col-md-1 px-0-right">
                                <div class="form-group">
                                    <p class="form-control-label text-center text-sm"> : </p>
                                </div>
                            </div>
                            <div class="col-md-7 px-0">
                                <div class="form-group">
                                    <p class="form-control-label text-left text-sm text-dark">PT.RJ Abadi</p>
                                </div>
                            </div>
                        </li>
                        <li class="row col-12">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <strong class="text-left text-sm text-dark"> Nomor Telepon </strong>
                                </div>
                            </div>
                            <div class="col-md-1 px-0-right">
                                <div class="form-group">
                                    <p class="form-control-label text-center text-sm"> : </p>
                                </div>
                            </div>
                            <div class="col-md-7 px-0">
                                <div class="form-group">
                                    <p class="form-control-label text-left text-sm text-dark">+(62) 123 1234 123</p>
                                </div>
                            </div>
                        </li>
                        <li class="row col-12">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <strong class="text-left text-sm text-dark"> Email </strong>
                                </div>
                            </div>
                            <div class="col-md-1 px-0-right">
                                <div class="form-group">
                                    <p class="form-control-label text-center text-sm"> : </p>
                                </div>
                            </div>
                            <div class="col-md-7 px-0">
                                <div class="form-group">
                                    <p class="form-control-label text-left text-sm text-dark">rjabadi@gmail.com</p>
                                </div>
                            </div>
                        </li>
                        <li class="row col-12">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <strong class="text-left text-sm text-dark"> Alamat Kantor </strong>
                                </div>
                            </div>
                            <div class="col-md-1 px-0-right">
                                <div class="form-group">
                                    <p class="form-control-label text-center text-sm"> : </p>
                                </div>
                            </div>
                            <div class="col-md-7 px-0">
                                <div class="form-group">
                                    <p class="form-control-label text-left text-sm text-dark">Kecamatan Depok, Kab. Sleman, DIY Jogjakarta</p>
                                </div>
                            </div>
                        </li>
                            <!-- <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Mobile:</strong>
                                &nbsp; +(62) 123 1234 123</li>
                            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Email:</strong>
                                &nbsp; rjabadi@gmail.com</li>
                            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Location:</strong>
                                &nbsp; Depok</li>
                            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Location:</strong>
                                &nbsp; Depok</li> -->
                        </ul>
                    </div>
                </div>
                <!-- kartu tambang -->
                </div class="col-lg-6 d-flex mx-auto center">
                    <div class="col-xl-5 mb-xl-0 mb-3 p-2 mx-auto">
                        <div class="card bg-transparent shadow-xl">
                            <div class="overflow-hidden position-relative border-radius-xl"
                                style="background-image: url('../assets/img/curved-images/batik4.jpg');">
                                <span class="mask bg-gradient-dark"></span>
                                <div class="card-body position-relative z-index-1 p-3">
                                    <i class="fas fa-wifi text-white p-2"></i>
                                    <h5 class="text-white mt-4 mb-5 pb-2">
                                        4562&nbsp;&nbsp;&nbsp;1122&nbsp;&nbsp;&nbsp;4594&nbsp;&nbsp;&nbsp;7852</h5>
                                    <div class="d-flex">
                                        <div class="d-flex">
                                            <div class="me-4">
                                                <p class="text-white text-sm opacity-8 mb-0">Card Holder</p>
                                                <h6 class="text-white mb-0">PT. RJ Abadi</h6>
                                            </div>
                                            <div>
                                                <p class="text-white text-sm opacity-8 mb-0">Expires</p>
                                                <h6 class="text-white mb-0">11/22</h6>
                                            </div>
                                        </div>
                                        <div class="ms-auto w-20 d-flex align-items-end justify-content-end">
                                                                <img class="w-60 mt-2" src="../assets/img/logos/mastercard.png" alt="logo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <!-- cadangan tambang -->
                <div class="row">
                    <!-- cadangan tambang 1 -->
                    <div class=" mt-4 col-lg-6">
                        <div class="card">
                            <!-- header informasi -->
                            <div class="card-header pb-0 p-3">
                                <div class="row">
                                    <div class="col-md-8 mb-0">
                                        <h5 class="">Cadangan Tambang 1</h5>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <a href="javascript:;">
                                            <i class="fas fa-user-edit text-secondary text-sm" data-bs-toggle="tooltip"
                                                data-bs-placement="top" title="Edit Profile"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- body informasi -->
                            <div class="card-body">
                                <ul class="list-group">
                                <li class="row col-12">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <strong class="text-left text-sm text-dark"> Lokasi Tambang </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-1 px-0-right">
                                        <div class="form-group">
                                            <p class="form-control-label text-center text-sm"> : </p>
                                        </div>
                                    </div>
                                    <div class="col-md-7 px-0">
                                        <div class="form-group">
                                            <p class="form-control-label text-left text-sm text-dark">Desa Maguwoharjo</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="row col-12">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <strong class="text-left text-sm text-dark"> Latitude </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-1 px-0-right">
                                        <div class="form-group">
                                            <p class="form-control-label text-center text-sm"> : </p>
                                        </div>
                                    </div>
                                    <div class="col-md-7 px-0">
                                        <div class="form-group">
                                            <p class="form-control-label text-left text-sm text-dark">-6.385589</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="row col-12">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <strong class="text-left text-sm text-dark"> Longitude </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-1 px-0-right">
                                        <div class="form-group">
                                            <p class="form-control-label text-center text-sm"> : </p>
                                        </div>
                                    </div>
                                    <div class="col-md-7 px-0">
                                        <div class="form-group">
                                            <p class="form-control-label text-left text-sm text-dark">106.830711</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="row col-12">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <strong class="text-left text-sm text-dark"> Cadangan Tambang </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-1 px-0-right">
                                        <div class="form-group">
                                            <p class="form-control-label text-center text-sm"> : </p>
                                        </div>
                                    </div>
                                    <div class="col-md-7 px-0">
                                        <div class="form-group">
                                        <div class="progress-wrapper w-95">
                                            <div class="progress-gradient-success">
                                                <div class="progress-percentage">
                                                <span class="text-xs font-weight-bold">20%</span>
                                                </div>
                                            </div>
                                            <div class="progress">
                                                <div class="progress-bar bg-gradient-success w-20" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- cadangan tambang 2 -->
                    <div class=" mt-4 col-lg-6">
                        <div class="card">
                            <!-- header informasi -->
                            <div class="card-header pb-0 p-3">
                                <div class="row">
                                    <div class="col-md-8 mb-0">
                                        <h5 class="">Cadangan Tambang 2</h5>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <a href="javascript:;">
                                            <i class="fas fa-user-edit text-secondary text-sm" data-bs-toggle="tooltip"
                                                data-bs-placement="top" title="Edit Profile"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- body informasi -->
                            <div class="card-body">
                                <ul class="list-group">
                                <li class="row col-12">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <strong class="text-left text-sm text-dark"> Lokasi Tambang </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-1 px-0-right">
                                        <div class="form-group">
                                            <p class="form-control-label text-center text-sm"> : </p>
                                        </div>
                                    </div>
                                    <div class="col-md-7 px-0">
                                        <div class="form-group">
                                            <p class="form-control-label text-left text-sm text-dark">Desa Kepuharjo</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="row col-12">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <strong class="text-left text-sm text-dark"> Latitude </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-1 px-0-right">
                                        <div class="form-group">
                                            <p class="form-control-label text-center text-sm"> : </p>
                                        </div>
                                    </div>
                                    <div class="col-md-7 px-0">
                                        <div class="form-group">
                                            <p class="form-control-label text-left text-sm text-dark">-2.467108</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="row col-12">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <strong class="text-left text-sm text-dark"> Longitude </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-1 px-0-right">
                                        <div class="form-group">
                                            <p class="form-control-label text-center text-sm"> : </p>
                                        </div>
                                    </div>
                                    <div class="col-md-7 px-0">
                                        <div class="form-group">
                                            <p class="form-control-label text-left text-sm text-dark">122.564900</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="row col-12">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <strong class="text-left text-sm text-dark"> Cadangan Tambang </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-1 px-0-right">
                                        <div class="form-group">
                                            <p class="form-control-label text-center text-sm"> : </p>
                                        </div>
                                    </div>
                                    <div class="col-md-7 px-0">
                                        <div class="form-group">
                                        <div class="progress-wrapper w-95">
                                            <div class="progress-gradient-danger">
                                                <div class="progress-percentage">
                                                <span class="text-xs font-weight-bold">60%</span>
                                                </div>
                                            </div>
                                            <div class="progress">
                                                <div class="progress-bar bg-gradient-danger w-60" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                    <!-- <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Mobile:</strong>
                                        &nbsp; +(62) 123 1234 123</li>
                                    <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Email:</strong>
                                        &nbsp; rjabadi@gmail.com</li>
                                    <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Location:</strong>
                                        &nbsp; Depok</li>
                                    <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Location:</strong>
                                        &nbsp; Depok</li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- tambang 1 -->
                <div class="row pt-4 pb-4">
                    <div class="col-12">
                            <div class="card">
                                    <div class="card-header pb-0 p-3">
                                        <div class="d-flex flex-row justify-content-between">
                                            <div class="col-md-3">
                                                <h5 class="mb-0">Transaksi Tambang I</h5>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="row col-sm-12">
                                                    <!-- date -->
                                                    <div class="col-md-2 offset-5 px-0 pr-0">
                                                    <div class="input-group">
                                                                <!-- <span class="input-group-text text-body"><i class="fas fa-calendar text-center" aria-hidden="true"></i></span> -->
                                                                <input class="form-control bg-white" type="date" id="tanggal" name="tanggal" data-toggle="tooltip" title="Pilih Tanggal">
                                                            </div>
                                                    </div>
                                                    <!-- button dropdown Waktu -->
                                                    <div class="col-md-2 px-0-left pr-0-right">
                                                            <div class="input-group">
                                                                <!-- <span class="input-group-text text-body">Waktu</span> -->
                                                                <input type="time" class="form-control bg-white" placeholder="plat" data-toggle="tooltop" title="Masukkan Waktu"></input>
                                                            </div>
                                                    </div>
                                                    <!-- button dropdown Plat Kendaraan -->
                                                    <div class="col-md-2 px-0 pr-0 text-center">
                                                        <div class="d-flex align-items-center">
                                                            <div class="input-group">
                                                                <span class="input-group-text text-body">Plat</span>
                                                                <input type="text" class="form-control bg-white" placeholder="Tulis Disini...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- button dropdown ijin tambang -->
                                                    <!-- <div class="col-md-3 px-1-left pr-0 text-center">
                                                        <div class="d-flex align-items-center">
                                                            <div class="input-group">
                                                                <span class="input-group-text text-body">Ijin Tambang</span>
                                                                <select type="text" class="form-control bg-white" placeholder="Pilih...">
                                                                <option selected>Pilih..</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    <!-- button cari -->
                                                    <div class="col-md-1 px-0-left pr-0 d-flex justify-content-end">
                                                        <a href="" class="input-group-text text-body bg-danger"><i class="fas fa-search text-center text-white" aria-hidden="true"></i></a>
                                                    </div>
                                                    <!-- <a href="#" class="btn bg-gradient-primary btn-sm mb-0" type="button">+&nbsp; New User</a> -->
                                                </div>
                                            </div>
                                            <!-- <a href="#" class="btn bg-gradient-primary btn-sm mb-0" type="button">+&nbsp; New User</a> -->
                                        </div>
                                    </div>
                                    <div class="card-body px-0 pt-0 pb-2">
                                        <div class="table-responsive p-0">
                                            <table class="table align-items-center mb-0">
                                                <thead>
                                                    <tr>
                                                        <th class="text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Nomor
                                                        </th>
                                                        <!-- <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                            Photo
                                                        </th> -->
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Tanggal
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Waktu
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Plat Kendaraan
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Harga/Ton
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Status
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Action
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!-- 1 -->
                                                    <tr>
                                                        <td class="ps-4">
                                                                    <p class="text-xs font-weight-bold mb-0">1</p>
                                                        </td>
                                                                <!-- <td>
                                                                    <div>
                                                                        <img src="../assets/img/SIP-BANG.png" class="avatar avatar-sm me-3">
                                                                    </div>
                                                                </td> -->
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rabu, 03/11/2021</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">08.50.00 WIB</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">AB 1234 CD</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rp 950.000</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <span class="badge badge-sm bg-success text-capitalize each word"> in </span>
                                                                </td>
                                                                <td class="text-center">
                                                                    <!-- tombol file -->
                                                                    <a href="#" class="mx-3" data-bs-toggle="modal" data-bs-target="#exampleModal" data-toggle="tooltip" title="Lihat Detail" class="red-tooltip">
                                                                        <i class="fas fa-file text-info"></i>
                                                                    </a>

                                                                    <!-- Modal file-->
                                                                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <div class="row col-12">
                                                                                    <div class="col-md-6 text-left">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                                                                                        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                                                                                    </div>
                                                                                    <div class="col-md-6 text-right">
                                                                                        <small class="text-danger">Status : Belum Di Cek <i class="fas fa-exclamation-circle text-danger"></i></small>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <form role="form text-left">
                                                                                    <!-- Tanggal dan Waktu -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Tanggal dan Waktu </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">Rabu, 03/11/2021 08.50.00 WIB</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- nama pt -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Nama PT </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">PT. RJ Abadi</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Plat Kendaraan-->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Plat Kendaraan </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">AB 1234 CD</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Harga/Ton -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Harga/Ton</p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">Rp 950.000</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- kir kendaraan-->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Kir Kendaraan </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6 px-0">
                                                                                            <div class="form-group">
                                                                                            <p class="form-control-label text-left text-sm text-dark"> 3.000 kg </p>
                                                                                                <!-- <input class="form-control form-control-sm" type="text" placeholder="3.000 kg"> -->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Jenisgalian -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Jenis Galian </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- tonasegalian -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Tonase Galian</p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- pajak -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Pajak </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- total Harga -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Total Harga </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Status -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left"> Status </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group text-left">
                                                                                                <p class="badge badge-sm bg-success text-capitalize each word ">In</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Tutup</button>
                                                                                <button type="button" class="btn btn-success">Cek Sekarang</button>
                                                                            </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <!-- tombol hapus -->
                                                                    <a href="#" data-bs-toggle="tooltip"
                                                                        data-bs-original-title="Hapus">
                                                                        <i class="cursor-pointer fas fa-trash text-danger"></i>
                                                                    </a>
                                                                </td>
                                                    </tr>

                                                    <!-- 2 -->
                                                            <tr>
                                                                <td class="ps-4">
                                                                    <p class="text-xs font-weight-bold mb-0">2</p>
                                                                </td>
                                                                <!-- <td>
                                                                    <div>
                                                                        <img src="../assets/img/SIP-BANG.png" class="avatar avatar-sm me-3">
                                                                    </div>
                                                                </td> -->
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rabu, 03/11/2021</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">06.45.00 WIB</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">AB 5678 FT</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rp 950.000</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <span class="badge badge-sm bg-success text-capitalize each word"> in </span>
                                                                </td>
                                                                <td class="text-center">
                                                                    <!-- tombol file -->
                                                                    <a href="#" class="mx-3" data-bs-toggle="modal" data-bs-target="#exampleModal2" data-toggle="tooltip" title="Lihat Detail" class="red-tooltip">
                                                                        <i class="fas fa-file text-info"></i>
                                                                    </a>

                                                                    <!-- Modal file-->
                                                                    <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <div class="row col-12">
                                                                                    <div class="col-md-6 text-left">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                                                                                        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                                                                                    </div>
                                                                                    <div class="col-md-6 text-right">
                                                                                        <small class="text-info">Status : Sudah Di Cek <i class="fas fa-check-circle text-info"></i></small>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <form role="form text-left">
                                                                                    <!-- Tanggal dan Waktu -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Tanggal dan Waktu </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">Rabu, 03/11/2021 06.45.00 WIB</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- nama pt -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Nama PT </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">PT. RJ Abadi</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Plat Kendaraan-->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Plat Kendaraan </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">AB 5678 FT</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Harga/Ton -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Harga/Ton</p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">Rp 950.000</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- kir kendaraan-->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Kir Kendaraan </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6 px-0">
                                                                                            <div class="form-group">
                                                                                            <p class="form-control-label text-left text-sm text-dark"> 3.000 kg </p>
                                                                                                <!-- <input class="form-control form-control-sm" type="text" placeholder="3.000 kg"> -->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Jenisgalian -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Jenis Galian </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- tonasegalian -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Tonase Galian</p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- pajak -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Pajak </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- total Harga -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Total Harga </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Status -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left"> Status </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group text-left">
                                                                                                <p class="badge badge-sm bg-success text-capitalize each word ">In</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Tutup</button>
                                                                                <!-- <button type="button" class="btn btn-success">Simpan</button> -->
                                                                            </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <!-- tombol hapus -->
                                                                    <a href="#" data-bs-toggle="tooltip"
                                                                        data-bs-original-title="Hapus">
                                                                        <i class="cursor-pointer fas fa-trash text-danger"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                            </div>
                    </div>
                </div>

                <!-- tambang 2 -->
                <div class="row pt-0">
                    <div class="col-12">
                            <div class="card">
                                    <div class="card-header pb-0 p-3">
                                        <div class="d-flex flex-row justify-content-between">
                                            <div class="col-md-3">
                                                <h5 class="mb-0">Transaksi Tambang II</h5>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="row col-sm-12">
                                                    <!-- date -->
                                                    <div class="col-md-2 offset-5 px-0 pr-0">
                                                    <div class="input-group">
                                                                <!-- <span class="input-group-text text-body"><i class="fas fa-calendar text-center" aria-hidden="true"></i></span> -->
                                                                <input class="form-control bg-white" type="date" id="tanggal" name="tanggal" data-toggle="tooltip" title="Pilih Tanggal">
                                                            </div>
                                                    </div>
                                                    <!-- button dropdown Waktu -->
                                                    <div class="col-md-2 px-0-left pr-0-right">
                                                            <div class="input-group">
                                                                <!-- <span class="input-group-text text-body">Waktu</span> -->
                                                                <input type="time" class="form-control bg-white" placeholder="plat" data-toggle="tooltop" title="Masukkan Waktu"></input>
                                                            </div>
                                                    </div>
                                                    <!-- button dropdown Plat Kendaraan -->
                                                    <div class="col-md-2 px-0 pr-0 text-center">
                                                        <div class="d-flex align-items-center">
                                                            <div class="input-group">
                                                                <span class="input-group-text text-body">Plat</span>
                                                                <input type="text" class="form-control bg-white" placeholder="Tulis Disini...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- button dropdown ijin tambang -->
                                                    <!-- <div class="col-md-3 px-1-left pr-0 text-center">
                                                        <div class="d-flex align-items-center">
                                                            <div class="input-group">
                                                                <span class="input-group-text text-body">Ijin Tambang</span>
                                                                <select type="text" class="form-control bg-white" placeholder="Pilih...">
                                                                <option selected>Pilih..</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    <!-- button cari -->
                                                    <div class="col-md-1 px-0-left pr-0 d-flex justify-content-end">
                                                        <a href="" class="input-group-text text-body bg-danger"><i class="fas fa-search text-center text-white" aria-hidden="true"></i></a>
                                                    </div>
                                                    <!-- <a href="#" class="btn bg-gradient-primary btn-sm mb-0" type="button">+&nbsp; New User</a> -->
                                                </div>
                                            </div>
                                            <!-- <a href="#" class="btn bg-gradient-primary btn-sm mb-0" type="button">+&nbsp; New User</a> -->
                                        </div>
                                    </div>
                                    <div class="card-body px-0 pt-0 pb-2">
                                        <div class="table-responsive p-0">
                                            <table class="table align-items-center mb-0">
                                                <thead>
                                                    <tr>
                                                        <th class="text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Nomor
                                                        </th>
                                                        <!-- <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                            Photo
                                                        </th> -->
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Tanggal
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Waktu
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Plat Kendaraan
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Harga/Ton
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Status
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                            Action
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!-- 1 -->
                                                    <tr>
                                                        <td class="ps-4">
                                                            <p class="text-xs font-weight-bold mb-0">1</p>
                                                        </td>
                                                        <!-- <td>
                                                            <div>
                                                                <img src="../assets/img/SIP-BANG.png" class="avatar avatar-sm me-3">
                                                            </div>
                                                        </td> -->
                                                        <td class="text-center">
                                                            <p class="text-xs font-weight-bold mb-0">Rabu, 03/11/2021</p>
                                                        </td>
                                                        <td class="text-center">
                                                            <p class="text-xs font-weight-bold mb-0">08.50.00 WIB</p>
                                                        </td>
                                                        <td class="text-center">
                                                            <p class="text-xs font-weight-bold mb-0">AB 1234 CD</p>
                                                        </td>
                                                        <td class="text-center">
                                                            <p class="text-xs font-weight-bold mb-0">Rp 950.000</p>
                                                        </td>
                                                        <td class="text-center">
                                                            <span class="badge badge-sm bg-success text-capitalize each word"> in </span>
                                                        </td>
                                                        <td class="text-center">
                                                            <a href="#" class="mx-3" data-bs-toggle="tooltip"
                                                                data-bs-original-title="Lihat Detail">
                                                                <i class="fas fa-file text-info"></i>
                                                            </a>
                                                            <a href="#" data-bs-toggle="tooltip"
                                                                data-bs-original-title="Hapus">
                                                                <i class="cursor-pointer fas fa-trash text-danger"></i>
                                                            </a>
                                                        </td>
                                                    </tr>

                                                    <!-- 2 -->
                                                    <tr>
                                                        <td class="ps-4">
                                                            <p class="text-xs font-weight-bold mb-0">2</p>
                                                        </td>
                                                        <!-- <td>
                                                            <div>
                                                                <img src="../assets/img/SIP-BANG.png" class="avatar avatar-sm me-3">
                                                            </div>
                                                        </td> -->
                                                        <td class="text-center">
                                                            <p class="text-xs font-weight-bold mb-0">Senin, 01/11/2021</p>
                                                        </td>
                                                        <td class="text-center">
                                                            <p class="text-xs font-weight-bold mb-0">09.18.00 WIB</p>
                                                        </td>
                                                        <td class="text-center">
                                                            <p class="text-xs font-weight-bold mb-0">AB 5678 FT</p>
                                                        </td>
                                                        <td class="text-center">
                                                            <p class="text-xs font-weight-bold mb-0">Rp 950.000</p>
                                                        </td>
                                                        <td class="text-center">
                                                            <span class="badge badge-sm bg-danger text-capitalize each word"> Selesai </span>
                                                        </td>
                                                        <td class="text-center">
                                                            <a href="#" class="mx-3" data-bs-toggle="tooltip"
                                                                data-bs-original-title="Lihat Detail">
                                                                <i class="fas fa-file text-info"></i>
                                                            </a>
                                                            <a href="#" data-bs-toggle="tooltip"
                                                                data-bs-original-title="Hapus">
                                                                <i class="cursor-pointer fas fa-trash text-danger"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>